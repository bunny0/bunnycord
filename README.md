# bunnycord

My set of personal theme patches for Discord's Android app.

<img src="img/preview.jpg" alt="preview" width="200"/>

Patches the theme and a couple strings only. Nothing else is changed. Check ![these instructions](https://gitdab.com/distok/cutthecord/src/branch/master/BUILDING.md) for how to use.

**Unbranded** - Retains same logo, name, and splash screen as the official app

**Branded** - Branded with bunny logo, splash screen, and app it renamed as BunnyCord :)

### Caveats

- Light theme doesn't work very well. Fixing it will require a lot of work
- Might be a couple versions behind the official release
- The following cutthecord patches will not work alongside the patches in this repo:
   - branding
   - customtheme
   - customversion

All other patches should work fine.

### Thanks

- ![cutthecord](https://gitdab.com/distok/cutthecord)
